package com.swagger.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.swagger.model.GetResponse;
import com.swagger.model.PostRequest;
import com.swagger.model.PostResponse;

@Service
public class SwaggerService {

	private static final Logger logger = LoggerFactory.getLogger(SwaggerService.class);

	public GetResponse responseGet() {

		logger.info("Hola Swagger");

		GetResponse response = new GetResponse();

		response.setNombre("Jose Luis");
		response.setApellidoPaterno("Rojas");
		response.setApellidoMaterno("Perez");
		response.setEdad("34");

		return response;
	}

	public PostResponse responsePost(PostRequest request) {

		PostResponse resultado = new PostResponse();

		resultado.setResultado(request.getNum1() + request.getNum2());

		return resultado;

	}

}
