package com.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSwaggerUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSwaggerUiApplication.class, args);
	}

}
