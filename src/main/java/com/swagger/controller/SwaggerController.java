package com.swagger.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.swagger.model.GetResponse;
import com.swagger.model.PostRequest;
import com.swagger.model.PostResponse;
import com.swagger.service.SwaggerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/swagger", produces = { "application/json" })
@Api(value = "Users microservice", description = "Ejemplo de diseño swagger")
public class SwaggerController {

	@Autowired
	private SwaggerService service;

	@RequestMapping(value = "/testGet", method = RequestMethod.GET)
	@ApiOperation(value = "Retorna mis datos", notes = "Retorna mis datos personales")
	public GetResponse testGet() throws IOException {

		return service.responseGet();

	}

	@RequestMapping(value = "/testPost", method = RequestMethod.POST)
	@ApiOperation(value = "Retorna mis datos", notes = "Retorna mis datos personales")
	public PostResponse testPost(@RequestBody PostRequest request) throws IOException {

		return service.responsePost(request);

	}
}
