package com.swagger.model;

import java.io.Serializable;

public class PostResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	int resultado;

	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}

}
